all: check

check: pylint doctest
	
pylint:
	pylint3 --version
	pylint3 --rcfile=pylintrc.ini SquaresFormation

doctest:
	python3 -mdoctest SquaresFormation/SquaresFormatter.py
